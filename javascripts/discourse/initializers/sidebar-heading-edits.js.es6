import { withPluginApi } from 'discourse/lib/plugin-api';
import { getLatestReplies } from 'discourse/plugins/discourse-sidebar-blocks/discourse/helpers/recent-replies';

function initializePlugin(api) {
    api.reopenWidget("sidebar-latest-replies",{
        refreshPosts() {
            if (this.state.loading) { return; }
            this.state.loading = true;
            this.state.posts = 'empty';
            getLatestReplies(this).then((result) => {
              if (result.length) {
                for (var i = result.length - 1; i >= 0; i--) {
                  // remove first post in a topic (not a reply)
                  // remove any "post" that is merely an action
                  // remove hidden posts

                  console.log(result[i].cooked);
                  if (result[i].post_number < 2 || result[i].action_code != undefined || result[i].hidden || $(result[i].cooked).text().length==0) {
                    result.splice(i, 1);
                  }
                }
        
                for (var i = result.length - 1; i >= 0; i--) {
                  // limit to 5 max
                  if (i > 6) {
                    result.splice(i, 1);
                  }
                }
                this.state.posts = result;
              } else {
                this.state.posts = 'empty'
              }
              this.state.loading = false
              this.scheduleRerender()
            })
          },
    });
}
export default
{
  name: 'sidebar-heading-edits',

  initialize(container)
  {
    withPluginApi('0.1', api => initializePlugin(api));
  }
};